package costencent

import (
	"github.com/tencentyun/cos-go-sdk-v5"
)

var optAppend = &cos.ObjectPutOptions{
	ObjectPutHeaderOptions: &cos.ObjectPutHeaderOptions{
		Listener: &cos.DefaultProgressListener{},
	},
}
