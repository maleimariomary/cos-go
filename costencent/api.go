package costencent

import (
	"context"
	"errors"
	"fmt"
	"github.com/tencentyun/cos-go-sdk-v5"
	"io"
	"net/http"
	"strings"
	"time"
)

func logStatus(err error) error {
	var errMsg strings.Builder

	if cos.IsNotFoundError(err) {
		// WARN
		errMsg.WriteString("WARN: Resource is not existed")
	} else if e, ok := cos.IsCOSError(err); ok {
		errMsg.WriteString(fmt.Sprintf("ERROR: Code: %v\n", e.Code))
		errMsg.WriteString(fmt.Sprintf("ERROR: Message: %v\n", e.Message))
		errMsg.WriteString(fmt.Sprintf("ERROR: Resource: %v\n", e.Resource))
		errMsg.WriteString(fmt.Sprintf("ERROR: RequestId: %v\n", e.RequestID))
		// ERROR
	} else {
		errMsg.WriteString(fmt.Sprintf("ERROR: %v\n", err))
		// ERROR
	}

	return errors.New(errMsg.String())
}

func AppendForm(name string, input *io.LimitedReader) error {
	optAppend.ContentLength = input.N

	c := New()
	pos, _, err := c.Object.Append(context.Background(), name, 0, input, optAppend)
	if err != nil {
		return logStatus(err)
	}

	fmt.Println(pos)

	return nil
}

func AppendLink(name, target string) error {
	client := &http.Client{Timeout: 100 * time.Second}

	req, err := http.NewRequest("GET", target, nil)
	if err != nil {
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	input := &io.LimitedReader{
		R: resp.Body, // underlying reader
		N: resp.ContentLength,
	}

	optAppend.ContentLength = input.N

	c := New()
	_, _, err = c.Object.Append(context.Background(), name, 0, input, optAppend)
	if err != nil {
		return logStatus(err)
	}

	return nil
}
