### COS Upload file by form and absoluted link

env config:

```
var (
	CosHost      = os.Getenv("CosHost")
	CosSecretID  = os.Getenv("CosSecretID")
	CosSecretKey = os.Getenv("CosSecretKey")
	CosProxyHost = os.Getenv("CosProxyHost")
	CosPort      = os.Getenv("CosPort")
)
```

💡 Notice that `CosPort` variable only load once, equal 9001 by default
