package controller

import (
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"gitee.com/maleimariomary/cos-go/costencent"
)

type PostParam struct {
	Animals []string
}

func Upload(c *gin.Context) {
	contentType := c.ContentType()

	if contentType != "multipart/form-data" {
		c.JSON(400, "incorrect content-type")
		return
	}

	fh, err := c.FormFile("attachment")
	if err != nil {
		c.JSON(400, fmt.Sprintf("formfile: %#v\n", err))
		return
	}

	fh.Filename = "permanent/" + strconv.Itoa(int(time.Now().UnixNano())) + "-" + fh.Filename

	file, err := fh.Open()
	if err != nil {
		c.JSON(500, fmt.Sprintf("open: %#v", err))
		return
	}
	defer file.Close()

	input := &io.LimitedReader{
		R: file,
		N: fh.Size,
	}

	err = costencent.AppendForm(fh.Filename, input)
	if err != nil {
		c.JSON(500, fmt.Sprintf("append-form: %#v", err))
		return
	}

	c.JSON(200, map[string]string{
		"Location": fmt.Sprintf("%s/%s", os.Getenv("CosProxyHost"), fh.Filename),
	})
}

func Link(c *gin.Context) {
	link := c.Query("link")
	if link == "" {
		c.JSON(400, "empty link")
		return
	}

	name := "temp/" + strconv.Itoa(int(time.Now().UnixNano())) + "-" + path.Base(link)

	err := costencent.AppendLink(name, link)
	if err != nil {
		c.JSON(500, fmt.Sprintf("AppendLink: %#v", err))
		return
	}

	c.JSON(200, map[string]string{
		"Location": fmt.Sprintf("%s/%s", os.Getenv("CosProxyHost"), name),
	})
}
