package main

import (
	"fmt"
	"gitee.com/maleimariomary/cos-go/controller"
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"strconv"
)

func CrossOrigin(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
}

func main() {
	gin.SetMode(gin.DebugMode)

	r := gin.Default()
	r.Use(CrossOrigin)

	r.POST("/Upload", controller.Upload)
	r.GET("/Link", controller.Link)

	port := 9001
	if cosPort := os.Getenv("CosPort"); cosPort != "" {
		var err error
		port, err = strconv.Atoi(cosPort)
		if err != nil {
			log.Println("incorrect env port")
		}
	}

	err := r.Run(fmt.Sprintf("0.0.0.0:%d", port))

	if err != nil {
		log.Fatal("Run: ", err)
	}
}
